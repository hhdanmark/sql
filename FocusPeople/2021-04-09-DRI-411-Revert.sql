ALTER TABLE Entitet
DROP COLUMN ActiveWhereClause
GO

ALTER VIEW [dbo].[ViewAktiveBevillinger]
AS
SELECT        BB.BrugerId, BB.Beskrivelse, CAST(BB.PeriodeStart AS date) AS Start, CAST(BB.PeriodeSlut AS date) AS Slut, BB.BevilgetTimer, BB.PuljeTimer, O.Tekst AS Lønmodel, BB.BrugerBevillingId, BB.OverenskomstId, 
                         BB.LoenGruppe AS StedTillæg, BB.LoenGruppeId - 1 AS Sted2
FROM            dbo.BrugerBevilling AS BB LEFT OUTER JOIN
                         dbo.Overenskomst AS O ON BB.OverenskomstId = O.Id
WHERE        (GETDATE() BETWEEN BB.PeriodeStart AND ISNULL(BB.PeriodeSlut, '2100-01-01')) AND (BB.BrugerId IN
                             (SELECT        ID
                               FROM            dbo.Bruger
                               WHERE        (Ophørt = 0)))
GO

ALTER VIEW [dbo].[ViewAktiveAnsættelser]
AS
SELECT DISTINCT 
                         H.ID AS HjælperID, H.Fornavne, H.Efternavn, H.EksternHjælper AS BPAhjælper, H.Akut, BHT.Fast, BHT.TimerPerUge, BHT.Løntrin, UT.Type, B.ID AS BrugerID, B.EksternBruger AS BPAbruger, ISNULL(RBT.Type, 
                         'Nej') AS Resp, CAST(BHT.Startdato AS date) AS Startdato, CAST(BHT.Slutdato AS date) AS Slutdato, BHT.LoenModel, o.Tekst, LGT.Takst AS Grundløn, BHT.Kvalifikationstillæg, H.OprettetTilPensionBPA, 
                         PL.PensionTekst AS PensionTekstBPA, H.OprettetTilPension AS OprettetTilPensionSundhed, PL2.PensionTekst AS PensionTekstSundhed
FROM            dbo.Hjælper AS H INNER JOIN
                         dbo.BrugerHjælperTilknytning AS BHT ON H.ID = BHT.HjælperID INNER JOIN
                         dbo.Bruger AS B ON B.ID = BHT.BrugerID LEFT OUTER JOIN
                         dbo.UddannelsesTyper AS UT ON UT.ID = BHT.UddannelsesType LEFT OUTER JOIN
                         dbo.RespiratorBrugerTyper AS RBT ON RBT.ID = B.RespiratorBrugerType LEFT OUTER JOIN
                         dbo.Overenskomst AS o ON o.Id = BHT.LoenModel LEFT OUTER JOIN
                         dbo.ViewAktiveBevillinger AS VAB ON VAB.BrugerId = B.ID LEFT OUTER JOIN
                         dbo.Loen_GrupperTrin AS LGT ON LGT.OverenskomstId = VAB.OverenskomstId AND LGT.Stedtillaeg = VAB.StedTillæg AND LGT.Loentrin = BHT.Løntrin LEFT OUTER JOIN
                         dbo.PensionsLoesninger AS PL ON PL.ID = H.PensionsLoesningBPA LEFT OUTER JOIN
                         dbo.PensionsLoesninger AS PL2 ON PL2.ID = H.PensionsLoesning
WHERE        (H.Uegnet = 0) AND (H.Passiv = 0) AND (B.Ophørt = 0) AND (GETDATE() BETWEEN BHT.Startdato AND ISNULL(BHT.Slutdato, '2100-01-01')) AND (H.ID > 1)
GO