USE FOCUS

ALTER TABLE Entitet
ADD ActiveWhereClause VARCHAR(500)
GO

--Use EntitetNavn instead of id in case an entitiy has different ids in different databases
UPDATE Entitet SET ActiveWhereClause='Ophørt=0' WHERE EntitetNavn='Bruger'
UPDATE Entitet SET ActiveWhereClause='Passiv=0 AND Uegnet=0' WHERE EntitetNavn='Hjælper'
UPDATE Entitet SET ActiveWhereClause='ID IN (SELECT AnsættelsesID FROM ViewAktiveAnsættelser)' WHERE EntitetNavn='Ansættelse'
UPDATE Entitet SET ActiveWhereClause='BrugerBevillingId IN (SELECT BrugerBevillingId FROM ViewAktiveBevillinger)' WHERE EntitetNavn='Bevilling'
INSERT INTO EntitetFilter (Query, DisplayNavn, EntitetID) VALUES ('1=1', 'Vis alle ansættelser', 9)
DELETE FROM EntitetFilter WHERE DisplayNavn='Vis tomme ansættelser'
GO

ALTER VIEW [dbo].[ViewAktiveBevillinger]
AS
SELECT BB.BrugerId, b.Fornavne, b.Efternavn, BB.Beskrivelse, CASE b.eksternbruger WHEN 1 THEN 'BPA' ELSE 'Helhed' END AS Type, 
	CAST(BB.PeriodeStart AS date) AS Start, CAST(BB.PeriodeSlut AS date) AS Slut, BB.BevilgetTimer, BB.PuljeTimer, 
	O.Tekst AS Lønmodel, BB.LoenTrin, BB.BrugerBevillingId, BB.OverenskomstId, 
	BB.LoenGruppe AS StedTillæg, BB.LoenGruppeId - 1 AS Sted2
FROM dbo.BrugerBevilling AS BB 
INNER JOIN dbo.Bruger AS b ON b.ID = BB.BrugerId 
LEFT OUTER JOIN dbo.Overenskomst AS O ON BB.OverenskomstId = O.Id
WHERE (GETDATE() BETWEEN BB.PeriodeStart AND ISNULL(BB.PeriodeSlut, '2100-01-01')) 
AND (BB.BrugerId IN (SELECT ID FROM dbo.Bruger WHERE Ophørt = 0))
GO

ALTER VIEW [dbo].[ViewAktiveAnsættelser]
AS
SELECT DISTINCT 
                         BHT.Id AS AnsættelsesID, H.ID AS HjælperID, H.Fornavne, H.Efternavn, H.EksternHjælper AS BPAhjælper, H.Akut, BHT.Fast, BHT.TimerPerUge, BHT.Løntrin, UT.Type, B.ID AS BrugerID, B.EksternBruger AS BPAbruger, ISNULL(RBT.Type, 
                         'Nej') AS Resp, CAST(BHT.Startdato AS date) AS Startdato, CAST(BHT.Slutdato AS date) AS Slutdato, BHT.LoenModel, o.Tekst, LGT.Takst AS Grundløn, BHT.Kvalifikationstillæg, H.OprettetTilPensionBPA, 
                         PL.PensionTekst AS PensionTekstBPA, H.OprettetTilPension AS OprettetTilPensionSundhed, PL2.PensionTekst AS PensionTekstSundhed
FROM            dbo.Hjælper AS H INNER JOIN
                         dbo.BrugerHjælperTilknytning AS BHT ON H.ID = BHT.HjælperID INNER JOIN
                         dbo.Bruger AS B ON B.ID = BHT.BrugerID LEFT OUTER JOIN
                         dbo.UddannelsesTyper AS UT ON UT.ID = BHT.UddannelsesType LEFT OUTER JOIN
                         dbo.RespiratorBrugerTyper AS RBT ON RBT.ID = B.RespiratorBrugerType LEFT OUTER JOIN
                         dbo.Overenskomst AS o ON o.Id = BHT.LoenModel LEFT OUTER JOIN
                         dbo.ViewAktiveBevillinger AS VAB ON VAB.BrugerId = B.ID LEFT OUTER JOIN
                         dbo.Loen_GrupperTrin AS LGT ON LGT.OverenskomstId = VAB.OverenskomstId AND LGT.Stedtillaeg = VAB.StedTillæg AND LGT.Loentrin = BHT.Løntrin LEFT OUTER JOIN
                         dbo.PensionsLoesninger AS PL ON PL.ID = H.PensionsLoesningBPA LEFT OUTER JOIN
                         dbo.PensionsLoesninger AS PL2 ON PL2.ID = H.PensionsLoesning
WHERE        (H.Uegnet = 0) AND (H.Passiv = 0) AND (B.Ophørt = 0) AND (GETDATE() BETWEEN BHT.Startdato AND ISNULL(BHT.Slutdato, '2100-01-01')) AND (H.ID > 1)
GO