ALTER TABLE Entitet
DROP COLUMN ActiveWhereClause
GO

ALTER VIEW [dbo].[ViewAktiveBevillinger]
AS
SELECT        BB.BrugerId, b.Fornavne, b.Efternavn, BB.Beskrivelse, CASE b.eksternbruger WHEN 1 THEN 'BPA' ELSE 'Helhed' END AS Type, CAST(BB.PeriodeStart AS date) AS Start, CAST(BB.PeriodeSlut AS date) AS Slut, 
                         BB.BevilgetTimer, BB.PuljeTimer, O.Tekst AS Lønmodel, BB.LoenTrin
FROM            dbo.BrugerBevilling AS BB INNER JOIN
                         dbo.Bruger AS b ON b.ID = BB.BrugerId LEFT OUTER JOIN
                         dbo.Overenskomst AS O ON BB.OverenskomstId = O.Id
WHERE        (GETDATE() BETWEEN BB.PeriodeStart AND ISNULL(BB.PeriodeSlut, '2100-01-01')) AND (b.Ophørt = 0) AND (b.ID NOT IN (312, 253))
GO

ALTER VIEW [dbo].[ViewAktiveAnsættelser]
AS
SELECT DISTINCT 
                         H.ID AS HjælperID, H.Fornavne, H.Efternavn, ISNULL(H.EksternHjælper, 0) AS BPAhjælper, H.Akut, BHT.Fast, BHT.TimerPerUge, BHT.Løntrin, UT.Type, B.ID AS BrugerID, B.Fornavne AS BrFornavne, 
                         B.Efternavn AS BrEfternavn, ISNULL(B.EksternBruger, 0) AS BPAbruger, ISNULL(RBT.Type, 'Nej') AS Resp, CAST(BHT.Startdato AS date) AS Startdato, CAST(BHT.Slutdato AS date) AS Slutdato
FROM            dbo.Hjælper AS H INNER JOIN
                         dbo.BrugerHjælperTilknytning AS BHT ON H.ID = BHT.HjælperID INNER JOIN
                         dbo.Bruger AS B ON B.ID = BHT.BrugerID LEFT OUTER JOIN
                         dbo.UddannelsesTyper AS UT ON UT.ID = BHT.UddannelsesType LEFT OUTER JOIN
                         dbo.RespiratorBrugerTyper AS RBT ON RBT.ID = B.RespiratorBrugerType
WHERE        (H.Uegnet = 0) AND (H.Passiv = 0) AND (B.Ophørt = 0) AND (GETDATE() BETWEEN BHT.Startdato AND ISNULL(BHT.Slutdato, '2100-01-01')) AND (H.ID > 1)
GO