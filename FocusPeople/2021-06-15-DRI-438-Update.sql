CREATE TABLE [dbo].[DokumentSkabelon](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[SkabelonId] [varchar](100) NOT NULL,
	[MappeId] [varchar](100) NULL,
	[DokumentNavneKonvention] [varchar](100) NOT NULL,
	[DokumentNavn] [varchar](100) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[UiComponent](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Name] [varchar](100) NOT NULL,
    [Url] [varchar](250) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[UiComponentInstance](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[UiComponentId] [int] NOT NULL FOREIGN KEY REFERENCES UiComponent(Id),
	[EntitetId] [int] NOT NULL UNIQUE FOREIGN KEY REFERENCES Entitet(EntitetId),
	[InsertionPoint] [varchar](100) NULL,
    [Name] [varchar](100) NOT NULL,
	[PrerequisiteSQL] [varchar](1000)

) ON [PRIMARY]
GO

CREATE TABLE [dbo].[UiComponentConfiguration](
	[UiComponentInstanceId] [int] NOT NULL FOREIGN KEY REFERENCES UiComponentInstance(Id),
	[Name] [varchar](100) NOT NULL,
    [Value] [varchar](250) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EntitetRelationer](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[EntitetId] [int] NOT NULL FOREIGN KEY REFERENCES Entitet(EntitetId),
	[RelationIdColumn] [varchar](100) NOT NULL,
	[Name] [varchar](100) NOT NULL
)
GO