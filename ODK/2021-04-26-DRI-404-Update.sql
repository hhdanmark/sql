USE FOCUS
GO

DECLARE @feltId INT

SELECT @feltId=FeltID FROM Metadata WHERE [Type] = 'table AnciennitetsreglerViewFunction(@userId)'

DELETE FROM FaneMetadata WHERE FeltID=@feltId
DELETE FROM Metadata WHERE [Type] = 'table AnciennitetsreglerViewFunction(@userId)'